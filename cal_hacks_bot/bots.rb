require 'twitter_ebooks'
require 'twitter'
require 'json'


# credit to twitter_ebooks for basically doing this hack for me lol

class MyBot < Ebooks::Bot
  # Configuration here applies to all MyBots
  attr_accessor :original, :model, :model_path, :ary, :current

  def configure
    self.consumer_key = 'q7T2x1iPfapSFPj6ZHr1u4lLr'
    self.consumer_secret = '4X4WsZ8gVNOjiBAOVMrPs49K8WOraKBi0fhMlKNFRqvu3iFD6d'

    # Users to block instead of interacting with
    self.blacklist = ['tnietzschequote']

    # Range in seconds to randomize delay when bot.delay is called
    self.delay_range = 1..6
  end

  def on_startup

    load_trends!
    statement = @trend.make_statement(140)
    tweet("TRENDING: #{statement}")
    load_tweeter!
    tweet("Pretending to be @#{@current} for the next 30 minutes!") 

    scheduler.every '1h' do
      # Tweet something every 24 hours
      # See https://github.com/jmettraux/rufus-scheduler
      # tweet("hi")
      # pictweet("hi", "cuteselfie.jpg")
      load_trends!
      statement = @trend.make_statement(140)
      tweet("TRENDING: #{statement}")
    end

    scheduler.every '30m' do
      load_tweeter!
      tweet("Pretending to be @#{@current} for the next 30 minutes!")
    end

    scheduler.every '5m' do 
      statement = @model.make_statement(140)
      tweet(statement)
    end

    # scheduler.every '2m' do 
    #   ryan_reply = model.make_statement(110)
    #   tweet("@ryanvailbrown " + ryan_reply)
    # end
  end

  def on_message(dm)
    # Reply to a DM
    # reply(dm, "secret secrets")
    statement = @model.make_statement(140)
    reply(dm, statement)
  end

  def on_follow(user)
    # Follow a user back
    # follow(user.screen_name)
  end

  def on_mention(tweet)
    # Reply to a mention
    # reply(tweet, "oh hullo")
    # reply(tweet, model.make_statement(120))
    reply(tweet, @model.make_statement)
  end

  def on_timeline(tweet)
    # Reply to a tweet in the bot's timeline
    reply(tweet, @model.make_statement)
  end

  private
  def load_model!
    return if @model

    @model_path ||= "model/#{original}.model"

    log "Loading model #{model_path}"
    @model = Ebooks::Model.load(@model_path)
  end

  private
  def load_tweeter!
    if ary.length == 0
      ary = IO.readlines("twitter.txt", "\n")
    end
    @current = ary[rand(100)].gsub("\n", '')
    json_path = "corpus/#{@current}.json"
    value = `ebooks archive #{@current} #{json_path}`
    value = `ebooks consume #{json_path}`
    @model_path = "model/#{@current}.model"

    log "Loading model #{model_path}"
    @model = Ebooks::Model.load(@model_path)
  end


  def make_client
      if File.exists?(CONFIG_PATH)
        @config = JSON.parse(File.read(CONFIG_PATH), symbolize_names: true)
      else
        @config = {}

        puts "As Twitter no longer allows anonymous API access, you'll need to enter the auth details of any account to use for archiving. These will be stored in #{CONFIG_PATH} if you need to change them later."
        print "Consumer key: "
        @config[:consumer_key] = STDIN.gets.chomp
        print "Consumer secret: "
        @config[:consumer_secret] = STDIN.gets.chomp
        print "Access token: "
        @config[:oauth_token] = STDIN.gets.chomp
        print "Access secret: "
        @config[:oauth_token_secret] = STDIN.gets.chomp

        File.open(CONFIG_PATH, 'w') do |f|
          f.write(JSON.pretty_generate(@config))
        end
      end

      Twitter::REST::Client.new do |config|
        config.consumer_key = @config[:consumer_key]
        config.consumer_secret = @config[:consumer_secret]
        config.access_token = @config[:oauth_token]
        config.access_token_secret = @config[:oauth_token_secret]
      end
    end


  private
  def load_trends!
    @path = "corpus/trends.json"
    if File.directory?(@path)
      @path = File.join(@path, "#{username}.json")
    end

    if File.exists?(@path)
      @tweets = JSON.parse(File.read(@path, :encoding => 'utf-8'), symbolize_names: true)
      log "Currently #{@tweets.length} tweets for #{@username}"
    else
      @tweets.nil?
      log "New archive for @#{username} at #{@path}"
    end

    @client = make_client
    array = @client.trends
    tweets = []
    array.each do |trend| 
      @client.search(trend.name).take(300).collect do |tweet|
        tweets.push(tweet)
      end
    end

    if tweets.length == 0
      log "No new tweets"
    else
      @tweets ||= []
      @tweets = tweets.map(&:attrs).each { |tw|
        tw.delete(:entities)
      } + @tweets
      File.open(@path, 'w') do |f|
        f.write(JSON.pretty_generate(@tweets))
      end
    end

    value = `ebooks consume corpus/trends.json`
    @trend_path = "model/trends.model"

    log "Loading model #{@trend_path}"
    @trend = Ebooks::Model.load(@trend_path)
  end


  

  
end
  

# Make a MyBot and attach it to an account
MyBot.new("InanHacks") do |bot|
  bot.access_token = "3853991119-g6cumfOAkhZ9ihbNHB9VHWkKtYP3DGAjUFn0WZs" # Token connecting the app to this account
  bot.access_token_secret = "KouPKhKimNMfzv133Mh4FiMmj7iZQePuF34A9jkHSWd53" # Secret connecting the app to this account
  bot.ary = Array.new
  bot.original = "Drake"
end